#!/bin/bash -euf

work_dir=$(dirname $(realpath $0))
stage=$1
stack_name=same-$stage

stacks=$(docker stack ls | grep $stack_name | wc -l)

if [ $stacks -ge 1 ]; then
  docker stack rm $stack_name
fi

while [ $(docker network ls | grep ${stack_name}_default | grep overlay | wc -l) -ge 1 ]; do
  echo "Waiting for docker network to be removed"
  sleep 1
done

cd $work_dir
docker stack deploy -c $work_dir/docker-compose.yml -c $work_dir/docker-compose_${stage}.yml $stack_name
