#!/bin/bash -xeuf

pipeline_dir=$(dirname $(realpath $0))
repo_dir=$(realpath $pipeline_dir/..)
ssh_dir=~/.ssh
private_key_target=$ssh_dir/id_ed25519
ssh_opts="-oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null"
remote_host="same@116.203.213.68"
stage=$1
deployment_dir=/opt/same/$stage
remote_script=update-on-server.sh

cp $repo_dir/discord/target/discord-0.0.1-SNAPSHOT-spring-boot.jar $pipeline_dir/discord.jar
cp $repo_dir/management/target/management-0.0.1-SNAPSHOT-spring-boot.jar $pipeline_dir/management.jar
cp $repo_dir/parser/target/parser-0.0.1-SNAPSHOT-spring-boot.jar $pipeline_dir/parser.jar
cp $repo_dir/mail/target/mail-0.0.1-SNAPSHOT-spring-boot.jar $pipeline_dir/mail.jar

mkdir -p $ssh_dir
cp $PRIVATE_KEY $private_key_target
chmod 700 $ssh_dir
chmod 600 $private_key_target
ssh-keygen -pf $private_key_target -P "$PRIVATE_KEY_PASSPHRASE" -N ""

ssh $ssh_opts $remote_host mkdir -p $deployment_dir

scp $ssh_opts \
    $pipeline_dir/discord.jar \
    $pipeline_dir/management.jar \
    $pipeline_dir/parser.jar \
    $pipeline_dir/mail.jar \
    $pipeline_dir/docker/docker-compose.yml \
    $pipeline_dir/docker/docker-compose_${stage}.yml \
    $pipeline_dir/$remote_script \
    $pipeline_dir/monitoring.sh \
    \
    same@116.203.213.68:$deployment_dir

ssh $ssh_opts $remote_host chmod u+x $deployment_dir/$remote_script
ssh $ssh_opts $remote_host $deployment_dir/$remote_script $stage
