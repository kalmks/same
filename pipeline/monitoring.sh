#!/bin/bash -xeuf

stage=$1
stack_name=same-$stage
desired_services=5
log=monitoring.log

touch $log

while [ true ]; do
  services=$(docker stack ps $stack_name | grep Running | wc -l)

  if [ $services -lt $desired_services ]; then
    date >> $log
    echo "Some services are down" >> $log
  fi

  sleep 10
done
