package com.kalmks.same.mail;

import com.kalmks.same.data.mail.SendMailRequestDto;
import com.kalmks.same.mail.config.domain.MailConfig;
import lombok.val;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
class MailService {

    private final MailConfig config;

    private final JavaMailSender mailSender;

    public MailService(MailConfig config, JavaMailSender mailSender) {
        this.config = config;
        this.mailSender = mailSender;
    }

    void send(final SendMailRequestDto request) {
        mailSender.send(buildMessage(request));
    }

    private SimpleMailMessage buildMessage(final SendMailRequestDto request) {
        val message = new SimpleMailMessage();
        message.setTo(request.getRecipient());
        message.setSubject(request.getSubject());
        message.setText(prefixMessage(request.getMessage()));

        return message;
    }

    private String prefixMessage(String message) {
        if (config.getSender().length() > 0) {
            message = config.getSender() + " " + message;
        }

        return message;
    }
}
