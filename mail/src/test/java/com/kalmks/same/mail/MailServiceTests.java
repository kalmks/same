package com.kalmks.same.mail;

import com.kalmks.same.data.mail.SendMailRequestDto;
import com.kalmks.same.mail.config.domain.MailConfig;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class MailServiceTests {

    @Mock
    private MailConfig config;

    @Mock
    private JavaMailSender mailSender;

    @InjectMocks
    private MailService service;

    @Test
    void send_shouldSendCorrectMessage() {
        val messageCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        val request = SendMailRequestDto
                .builder()
                .recipient("recipient@mail.tld")
                .subject("sub")
                .message("we need to build a wall")
                .build();

        given(config.getSender()).willReturn("");
        willDoNothing().given(mailSender).send(messageCaptor.capture());

        service.send(request);

        then(mailSender).should(times(1)).send(any(SimpleMailMessage.class));

        val message = messageCaptor.getValue();

        assertThat(message.getTo()).containsOnly(request.getRecipient());
        assertThat(message.getSubject()).isEqualTo(request.getSubject());
        assertThat(message.getText()).isEqualTo(request.getMessage());
    }

    @Test
    void send_shouldPrefixMessageCorrectly() {
        val messageCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        val request = SendMailRequestDto
                .builder()
                .message("we need to build a wall")
                .build();

        given(config.getSender()).willReturn("[prefix]");
        willDoNothing().given(mailSender).send(messageCaptor.capture());

        service.send(request);

        val message = messageCaptor.getValue();

        assertThat(message.getText()).isEqualTo("[prefix] " + request.getMessage());
    }
}
