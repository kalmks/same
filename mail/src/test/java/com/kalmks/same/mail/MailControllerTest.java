package com.kalmks.same.mail;

import com.kalmks.same.data.mail.SendMailRequestDto;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;

@ExtendWith(MockitoExtension.class)
class MailControllerTest {

    @Mock
    private MailService mailService;

    @InjectMocks
    private MailController controller;

    @Test
    void send_shouldUseMailService() {
        val requestCaptor = ArgumentCaptor.forClass(SendMailRequestDto.class);
        val request = SendMailRequestDto
                .builder()
                .recipient("recp")
                .subject("subj")
                .message("msg")
                .build();

        willDoNothing().given(mailService).send(requestCaptor.capture());

        controller.send(request);

        then(mailService).should().send(request);

        val capturedRequest = requestCaptor.getValue();

        assertThat(capturedRequest).isEqualTo(request);
    }
}
