CREATE TABLE IF NOT EXISTS product_history
(
    id          BIGINT AUTO_INCREMENT NOT NULL,
    item_number INT                   NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (item_number)
);

CREATE TABLE IF NOT EXISTS price
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    product_history_id BIGINT                NOT NULL,
    detection_date     DATETIME              NOT NULL,
    price_saturn       DECIMAL               NOT NULL,
    price_media_markt  DECIMAL               NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (product_history_id) REFERENCES product_history (id)
);

CREATE TABLE IF NOT EXISTS sale
(
    id                           BIGINT AUTO_INCREMENT NOT NULL,
    product_history_id           BIGINT                NOT NULL,
    detection_date               DATETIME              NOT NULL,
    discounted_price_saturn      DECIMAL               NOT NULL,
    discounted_price_media_markt DECIMAL               NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (product_history_id) REFERENCES product_history (id)
);

CREATE TABLE IF NOT EXISTS subscription
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    product_history_id BIGINT                NOT NULL,
    mail               VARCHAR               NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (product_history_id) REFERENCES product_history (id)
);
