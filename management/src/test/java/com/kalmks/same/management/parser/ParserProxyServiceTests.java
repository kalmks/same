package com.kalmks.same.management.parser;

import com.kalmks.same.data.parser.GetAttributeRequestDto;
import com.kalmks.same.data.parser.GetAttributeResponseDto;
import com.kalmks.same.data.parser.GetTextRequestDto;
import com.kalmks.same.data.parser.GetTextResponseDto;
import com.kalmks.same.management.config.domain.ParserConfig;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class ParserProxyServiceTests {

    private static Stream<Arguments> getAttribute_nullResponses() {
        return Stream.of(
                Arguments.of(null, "response is null"),
                Arguments.of(GetAttributeResponseDto.builder().result(null).build(), "result is null")
        );
    }

    private static Stream<Arguments> getText_nullResponses() {
        return Stream.of(
                Arguments.of(null, "response is null"),
                Arguments.of(GetTextResponseDto.builder().result(null).build(), "result is null")
        );
    }

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ParserConfig config;

    private ParserProxyService service;

    private final GetAttributeRequestDto attributeRequest = GetAttributeRequestDto
            .builder()
            .url("http://some.url")
            .cssSelector(".selector")
            .attribute("attr")
            .build();

    private final GetTextRequestDto textRequest = GetTextRequestDto
            .builder()
            .url("http://some.url")
            .cssSelector(".selector")
            .build();

    @BeforeEach
    void setUp() {
        given(restTemplateBuilder.build()).willReturn(restTemplate);
        given(config.getUrl()).willReturn("http://parser:8080");

        service = new ParserProxyService(restTemplateBuilder, config);
    }

    @Test
    void getAttribute_createsCorrectRequest() {
        val response = GetAttributeResponseDto.builder().result("foo").build();

        given(restTemplate.postForObject(getGetAttributeUrl(), attributeRequest, GetAttributeResponseDto.class)).willReturn(response);

        service.getAttribute(attributeRequest.getUrl(), attributeRequest.getCssSelector(), attributeRequest.getAttribute());

        then(restTemplate).should(times(1)).postForObject(getGetAttributeUrl(), attributeRequest, GetAttributeResponseDto.class);
    }

    @ParameterizedTest
    @MethodSource("getAttribute_nullResponses")
    void getAttribute_blockNullResponse(GetAttributeResponseDto response, String message) {
        given(restTemplate.postForObject(getGetAttributeUrl(), attributeRequest, GetAttributeResponseDto.class)).willReturn(response);

        assertThatThrownBy(() ->
                service.getAttribute(
                        attributeRequest.getUrl(),
                        attributeRequest.getCssSelector(),
                        attributeRequest.getAttribute()))
                .isInstanceOf(AssertionError.class)
                .hasMessage(message);
    }

    @Test
    void getText_createsCorrectRequest() {
        val response = GetTextResponseDto.builder().result("foo").build();

        given(restTemplate.postForObject(getGetTextUrl(), textRequest, GetTextResponseDto.class)).willReturn(response);

        service.getText(textRequest.getUrl(), textRequest.getCssSelector());

        then(restTemplate).should(times(1)).postForObject(getGetTextUrl(), textRequest, GetTextResponseDto.class);
    }

    @ParameterizedTest
    @MethodSource("getText_nullResponses")
    void getText_blockNullResponse(GetTextResponseDto response, String message) {
        given(restTemplate.postForObject(getGetTextUrl(), textRequest, GetTextResponseDto.class)).willReturn(response);

        assertThatThrownBy(() -> service.getText(textRequest.getUrl(), textRequest.getCssSelector()))
                .isInstanceOf(AssertionError.class)
                .hasMessage(message);
    }

    private String getGetAttributeUrl() {
        return config.getUrl() + "/rest/parser/get-attribute";
    }

    private String getGetTextUrl() {
        return config.getUrl() + "/rest/parser/get-text";
    }

}
