package com.kalmks.same.management.entity;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class SubscriptionRepositoryTest {

    public static final String MAIL = "my@mail.tld";

    @Autowired
    private SubscriptionRepository repository;

    @Autowired
    private ProductRepository productRepository;

    private ProductHistory productHistory;

    @BeforeEach
    void setUp() {
        productHistory = ProductHistory
                .builder()
                .itemNumber(0)
                .build();
        productHistory = productRepository.save(productHistory);
    }

    @Test
    public void canFindSavedEntity() {
        val subscription = Subscription
                .builder()
                .productHistory(productHistory)
                .mail(MAIL)
                .build();

        assertThat(repository.findAll())
                .noneMatch(it ->
                        it.getProductHistory().equals(productHistory)
                                && it.getMail().equals(MAIL));

        val savedSubscription = repository.save(subscription);

        assertThat(repository.findAll())
                .anyMatch(it ->
                        Objects.equals(it.getId(), savedSubscription.getId())
                                && it.getProductHistory().equals(productHistory)
                                && it.getMail().equals(MAIL));
    }

}
