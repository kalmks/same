package com.kalmks.same.management.entity;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class SaleRepositoryTests {

    public static final LocalDateTime DATE_TIME         = LocalDateTime.now();
    public static final BigDecimal    PRICE_SATURN      = BigDecimal.ONE;
    public static final BigDecimal    PRICE_MEDIA_MARKT = BigDecimal.ZERO;

    @Autowired
    private SaleRepository repository;

    @Autowired
    private ProductRepository productRepository;

    private ProductHistory productHistory;

    @BeforeEach
    void setUp() {
        productHistory = ProductHistory
            .builder()
            .itemNumber(0)
            .build();
        productHistory = productRepository.save(productHistory);
    }

    @Test
    public void canFindSavedEntity() {
        val sale = Sale
            .builder()
            .productHistory(productHistory)
            .detectionDate(DATE_TIME)
            .discountedPriceSaturn(PRICE_SATURN)
            .discountedPriceMediaMarkt(PRICE_MEDIA_MARKT)
            .build();

        assertThat(repository.findAll())
            .noneMatch(it ->
                           it.getProductHistory().equals(productHistory)
                               && it.getDetectionDate().equals(DATE_TIME)
                               && it.getDiscountedPriceSaturn().equals(PRICE_SATURN)
                               && it.getDiscountedPriceMediaMarkt().equals(PRICE_MEDIA_MARKT));

        val savedSale = repository.save(sale);

        assertThat(repository.findAll())
            .anyMatch(it ->
                          Objects.equals(it.getId(), savedSale.getId())
                              && it.getProductHistory().equals(productHistory)
                              && it.getDetectionDate().equals(DATE_TIME)
                              && it.getDiscountedPriceSaturn().equals(PRICE_SATURN)
                              && it.getDiscountedPriceMediaMarkt().equals(PRICE_MEDIA_MARKT));
    }

}
