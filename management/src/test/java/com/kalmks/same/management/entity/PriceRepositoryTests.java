package com.kalmks.same.management.entity;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class PriceRepositoryTests {

    public static final LocalDateTime DATE_TIME         = LocalDateTime.now();
    public static final BigDecimal    PRICE_SATURN      = BigDecimal.ZERO;
    public static final BigDecimal    PRICE_MEDIA_MARKT = BigDecimal.ONE;

    @Autowired
    private PriceRepository repository;

    @Autowired
    private ProductRepository productRepository;

    private ProductHistory productHistory;

    @BeforeEach
    void setUp() {
        productHistory = ProductHistory
            .builder()
            .itemNumber(0)
            .build();
        productHistory = productRepository.save(productHistory);
    }

    @Test
    public void canFindSavedEntity() {
        val price = Price
            .builder()
            .productHistory(productHistory)
            .detectionDate(DATE_TIME)
            .priceSaturn(PRICE_SATURN)
            .priceMediaMarkt(PRICE_MEDIA_MARKT)
            .build();

        assertThat(repository.findAll())
            .noneMatch(it ->
                           it.getProductHistory().equals(productHistory)
                               && it.getDetectionDate().equals(DATE_TIME)
                               && it.getPriceSaturn().equals(PRICE_SATURN)
                               && it.getPriceMediaMarkt().equals(PRICE_MEDIA_MARKT));

        val savedPrice = repository.save(price);

        assertThat(repository.findAll())
            .anyMatch(it ->
                          Objects.equals(it.getId(), savedPrice.getId())
                              && it.getProductHistory().equals(productHistory)
                              && it.getDetectionDate().equals(DATE_TIME)
                              && it.getPriceSaturn().equals(PRICE_SATURN)
                              && it.getPriceMediaMarkt().equals(PRICE_MEDIA_MARKT));
    }

}
