package com.kalmks.same.management.entity;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ProductHistoryRepositoryTests {

    private static final Integer ITEM_NUMBER = 12345;

    @Autowired
    private ProductRepository repository;

    @Test
    public void canFindEntityOnlyIfSaved() {
        val product = ProductHistory
            .builder()
            .itemNumber(ITEM_NUMBER)
            .build();

        assertThat(repository.findAll())
            .noneMatch(it -> it.getItemNumber().equals(ITEM_NUMBER));

        val savedProduct = repository.save(product);

        assertThat(repository.findAll())
            .anyMatch(it ->
                          Objects.equals(it.getId(), savedProduct.getId())
                              && it.getItemNumber().equals(ITEM_NUMBER));
    }

}
