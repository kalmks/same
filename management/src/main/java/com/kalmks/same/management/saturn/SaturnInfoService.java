package com.kalmks.same.management.saturn;

import com.kalmks.same.management.parser.ParserProxyService;
import lombok.val;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class SaturnInfoService {

    private ParserProxyService parserProxyService;

    public SaturnInfoService(final ParserProxyService parserProxyService) {
        this.parserProxyService = parserProxyService;
    }

    public String getName(final int artNo) {
        return parserProxyService.getText(
            getProductUrl(artNo),
            "body.page-product>div#product-wrapper>div#product-details>div.details>h1[itemprop='name']"
        );
    }

    public BigDecimal getPrice(final int artNo) {
        val price = parserProxyService.getAttribute(
            getProductUrl(artNo),
            "body.page-product>div#product-wrapper>div#product-details>div.price-sidebar>div.price-details>meta[itemprop='price']",
            "content"
        );

        return new BigDecimal(price);
    }

    public BigDecimal getDiscountedPrice(final int artNo) {
        var price = parserProxyService.getText(
            getProductUrl(artNo),
            "body.page-product>div#product-wrapper>div#product-details>div.price-sidebar>div.price-details.has-old-price>div.old-price-block>div.price.price-old"
        );

        price = price.replace("UVP ", "");

        return new BigDecimal(price);

    }

    private String getProductUrl(final int artNo) {
        return "https://www.saturn.de/de/search.html?query=" + artNo;
    }

}
