package com.kalmks.same.management.vo;

import com.kalmks.same.management.entity.ProductHistory;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Product {

    private Integer number;

    private String name;

    private ProductHistory history;
}
