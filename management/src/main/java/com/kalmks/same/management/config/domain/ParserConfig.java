package com.kalmks.same.management.config.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "same.parser")
public class ParserConfig {

    private String hostname;
    private int    port;

    public String getUrl() {
        return "http://" + hostname + ":" + port;
    }

}
