package com.kalmks.same.management.mediamarkt;

import com.kalmks.same.management.parser.ParserProxyService;
import lombok.val;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class MediaMarktInfoService {

    private ParserProxyService parserProxyService;

    public MediaMarktInfoService(final ParserProxyService parserProxyService) {
        this.parserProxyService = parserProxyService;
    }

    public String getName(final int artNo) {
        return parserProxyService.getText(
            getProductUrl(artNo),
            "body>div#root>div#rootWrapper>div>div>div>div>div>div>div[data-test='mms-select-details-header']>h1"
        );
    }

    public BigDecimal getPrice(final int artNo) {
        val price = parserProxyService.getAttribute(
            getProductUrl(artNo),
            "body>div#root>div#rootWrapper>div>div>div>div>div>div>div>div>div>div[itemprop='offers']>meta",
            "content"
        );

        return new BigDecimal(price);
    }

    public BigDecimal getDiscountedPrice(final int artNo) {
        var price = parserProxyService.getText(
            getProductUrl(artNo),
            "body>div#root>div#rootWrapper>div>div>div>div>div>div>div>div>div>div[itemprop='offers]>div>div>div>div>div>div>div>span"
        );

        price = price.replace("UVP ", "");

        return new BigDecimal(price);

    }

    private String getProductUrl(final int artNo) {
        val searchUrl = "https://www.mediamarkt.de/de/search.html?query=" + artNo;

        val productUrl = parserProxyService.getAttribute(
            searchUrl,
            "body>div#root>div#rootWrapper>div>div>div>div>div[data-test='mms-search-main']>div[data-test='mms-search-srp-productlist']>div>div[data-test='mms-search-srp-productlist-item']>a",
            "href"
        );

        return "https://www.mediamarkt.de/" + productUrl;
    }

}
