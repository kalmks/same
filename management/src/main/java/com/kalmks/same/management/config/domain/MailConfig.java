package com.kalmks.same.management.config.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "same.mail")
public class MailConfig {

    private String hostname;
    private int port;

    public String getUrl() {
        return "http://" + hostname + ":" + port;
    }

}
