package com.kalmks.same.management.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;

@MappedSuperclass
abstract class AbstractPersistable implements Persistable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Override
    @Transient
    public boolean isNew() {
        return getId() == null;
    }
}
