package com.kalmks.same.management.parser;

import com.kalmks.same.data.parser.GetAttributeRequestDto;
import com.kalmks.same.data.parser.GetAttributeResponseDto;
import com.kalmks.same.data.parser.GetTextRequestDto;
import com.kalmks.same.data.parser.GetTextResponseDto;
import com.kalmks.same.management.config.domain.ParserConfig;
import lombok.val;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ParserProxyService {
    private final RestTemplate restTemplate;
    private final ParserConfig config;

    ParserProxyService(final RestTemplateBuilder restTemplateBuilder, final ParserConfig config) {
        restTemplate = restTemplateBuilder.build();
        this.config = config;
    }

    public String getAttribute(final String url, final String selector, final String attribute) {
        val request = GetAttributeRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .attribute(attribute)
            .build();
        val response = restTemplate.postForObject(getGetAttributeUrl(), request, GetAttributeResponseDto.class);

        assert response != null : "response is null";
        assert response.getResult() != null : "result is null";

        return response.getResult();
    }

    public String getText(final String url, final String selector) {
        val request = GetTextRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .build();
        val response = restTemplate.postForObject(getGetTextUrl(), request, GetTextResponseDto.class);

        assert response != null : "response is null";
        assert response.getResult() != null : "result is null";

        return response.getResult();
    }

    private String getGetAttributeUrl() {
        return config.getUrl() + "/rest/parser/get-attribute";
    }

    private String getGetTextUrl() {
        return config.getUrl() + "/rest/parser/get-text";
    }
}
