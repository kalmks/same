package com.kalmks.same.management.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Subscription extends AbstractPersistable {

    @ManyToOne
    @JoinColumn(name = "product_history_id")
    private ProductHistory productHistory;

    private String mail;

}
