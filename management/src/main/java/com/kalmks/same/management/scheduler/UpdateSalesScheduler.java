package com.kalmks.same.management.scheduler;

import com.kalmks.same.management.entity.*;
import com.kalmks.same.management.mail.MailProxyService;
import com.kalmks.same.management.saturn.SaturnInfoService;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Configuration
@EnableScheduling
public class UpdateSalesScheduler {

    private static Logger logger = LoggerFactory.getLogger(UpdateSalesScheduler.class);

    private ProductRepository productRepository;

    private PriceRepository priceRepository;

    private SaleRepository saleRepository;

    private SaturnInfoService saturnInfoService;

    private MailProxyService mailProxyService;

    public UpdateSalesScheduler(
            ProductRepository productRepository,
            PriceRepository priceRepository,
            SaleRepository saleRepository,
            SaturnInfoService saturnInfoService) {
        this.productRepository = productRepository;
        this.priceRepository = priceRepository;
        this.saleRepository = saleRepository;
        this.saturnInfoService = saturnInfoService;
    }

    @Scheduled(cron = "0 0 */4 * * *")
    public void scheduleFixedDelayTask() {
        logger.info("Starting to update prices and sales");

        productRepository
                .findAll()
                .forEach(this::updateProductInformation);

        logger.info("Finished to update prices and sales");
    }

    private void updateProductInformation(final ProductHistory productHistory) {
        val artNo = productHistory.getItemNumber();

        val price = saturnInfoService.getPrice(artNo);
        val discountedPrice = saturnInfoService.getDiscountedPrice(artNo);

        val latestPrice = getLatest(productHistory.getPrices(), Comparator.comparing(Price::getDetectionDate));
        val latestDiscountedPrice = getLatest(productHistory.getSales(), Comparator.comparing(Sale::getDetectionDate));

        val priceUpdated = price.compareTo(latestPrice.getPriceSaturn()) != 0;
        val discountedPriceUpdated = discountedPrice.compareTo(latestDiscountedPrice.getDiscountedPriceSaturn()) == 0;

        if (priceUpdated) {
            val newPrice = Price
                    .builder()
                    .productHistory(productHistory)
                    .detectionDate(LocalDateTime.now())
                    .priceSaturn(price)
                    .priceMediaMarkt(BigDecimal.ZERO)
                    .build();

            priceRepository.save(newPrice);

            productHistory
                    .getSubscriptions()
                    .forEach(this::notifySubscribers);
        }

        if (discountedPriceUpdated) {
            val newSale = Sale
                .builder()
                .productHistory(productHistory)
                .detectionDate(LocalDateTime.now())
                .discountedPriceSaturn(price)
                .build();

            saleRepository.save(newSale);
        }
    }

    private void notifySubscribers(Subscription subscription) {
        val artNo = subscription.getProductHistory().getItemNumber();
        val price = getLatest(subscription.getProductHistory().getPrices(), Comparator.comparing(Price::getDetectionDate)).getPriceSaturn();

        mailProxyService.send(subscription.getMail(), artNo.toString() + " price updated", "This articles price was updated to " + price);
    }

    private <T> T getLatest(final List<T> list, Comparator<? super T> comparator) {
        if (list.size() <= 0) {
            return null;
        }

        list.sort(comparator);

        return list.get(0);
    }
}
