package com.kalmks.same.management.saturn;

import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/rest/saturn")
public class SaturnInfoController {

    private final SaturnInfoService saturnInfoService;

    public SaturnInfoController(SaturnInfoService saturnInfoService) {
        this.saturnInfoService = saturnInfoService;
    }

    @GetMapping("/{artNo}/name")
    String getName(@PathVariable("artNo") final Integer artNo) {
        return saturnInfoService.getName(artNo);
    }

    @GetMapping("/{artNo}/price")
    BigDecimal getPrice(@PathVariable("artNo") final Integer artNo) {
        return saturnInfoService.getPrice(artNo);
    }

}
