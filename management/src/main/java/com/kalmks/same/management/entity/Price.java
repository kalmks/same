package com.kalmks.same.management.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Price extends AbstractPersistable {

    @ManyToOne
    @JoinColumn(name = "product_history_id")
    private ProductHistory productHistory;

    private LocalDateTime detectionDate;

    private BigDecimal priceSaturn;

    private BigDecimal priceMediaMarkt;

}
