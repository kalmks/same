package com.kalmks.same.management.subscription;

import com.kalmks.same.data.management.AddSubscriptionRequestDto;
import com.kalmks.same.management.entity.ProductHistory;
import com.kalmks.same.management.entity.ProductRepository;
import com.kalmks.same.management.entity.Subscription;
import com.kalmks.same.management.entity.SubscriptionRepository;
import lombok.val;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/subscription")
public class SubscriptionController {

    private final SubscriptionRepository subscriptionRepository;

    private final ProductRepository productRepository;

    public SubscriptionController(final SubscriptionRepository subscriptionRepository, final ProductRepository productRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.productRepository = productRepository;
    }

    @PostMapping("/add")
    void add(
        @RequestBody
        final AddSubscriptionRequestDto request
    ) {
        assert request.getArtNo() != null;
        assert request.getMail() != null;

        var productHistory = productRepository.findOneByItemNumber(request.getArtNo());

        if (productHistory == null) {
            productHistory = ProductHistory
                .builder()
                .itemNumber(request.getArtNo())
                .build();

            productHistory = productRepository.save(productHistory);
        }

        val subscription = Subscription
            .builder()
            .productHistory(productHistory)
            .mail(request.getMail())
            .build();

        subscriptionRepository.save(subscription);
    }

}
