package com.kalmks.same.management.entity;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<ProductHistory, Long> {

    public ProductHistory findOneByItemNumber(Integer itemNumber);

}
