package com.kalmks.same.management.mail;

import com.kalmks.same.data.mail.SendMailRequestDto;
import com.kalmks.same.data.mail.SendMailResponseDto;
import com.kalmks.same.management.config.domain.MailConfig;
import lombok.val;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MailProxyService {
    private final RestTemplate restTemplate;
    private final MailConfig config;

    MailProxyService(final RestTemplateBuilder restTemplateBuilder, final MailConfig config) {
        restTemplate = restTemplateBuilder.build();
        this.config = config;
    }

    public void send(final String recipient, final String subject, final String message) {
        val request = SendMailRequestDto
                .builder()
                .recipient(recipient)
                .subject(subject)
                .message(message)
                .build();
        val response = restTemplate.postForObject(getSendUrl(), request, SendMailResponseDto.class);

        assert response != null : "response is null";
    }

    private String getSendUrl() {
        return config.getUrl() + "/rest/mail/send";
    }
}
