package com.kalmks.same.management.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductHistory extends AbstractPersistable {

    private Integer itemNumber;

    @OneToMany(mappedBy = "productHistory")
    private List<Price> prices;

    @OneToMany(mappedBy = "productHistory")
    private List<Sale> sales;

    @OneToMany(mappedBy = "productHistory")
    private List<Subscription> subscriptions;

}
