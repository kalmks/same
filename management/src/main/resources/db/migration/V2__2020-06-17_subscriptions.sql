CREATE TABLE subscription
(
    id                 BIGSERIAL NOT NULL,
    product_history_id BIGINT    NOT NULL
        CONSTRAINT subscription_product_id_fk REFERENCES product_history (id) ON DELETE RESTRICT,
    mail               VARCHAR   NOT NULL,
    PRIMARY KEY (id)
);
