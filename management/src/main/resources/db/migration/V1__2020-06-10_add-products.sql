CREATE TABLE product_history (
    id          BIGSERIAL NOT NULL,
    item_number INT       NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (item_number)
);

CREATE TABLE price (
    id                 BIGSERIAL NOT NULL,
    product_history_id BIGINT    NOT NULL
        CONSTRAINT price_product_history_id_fk REFERENCES product_history (id) ON DELETE RESTRICT,
    detection_date     TIMESTAMP NOT NULL,
    price_saturn       NUMERIC   NOT NULL,
    price_media_markt  NUMERIC   NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sale (
    id                           BIGSERIAL NOT NULL,
    product_history_id           BIGINT    NOT NULL
        CONSTRAINT sale_product_history_id_fk REFERENCES product_history (id) ON DELETE RESTRICT,
    detection_date               TIMESTAMP NOT NULL,
    discounted_price_saturn      NUMERIC   NOT NULL,
    discounted_price_media_markt NUMERIC   NOT NULL,
    PRIMARY KEY (id)
);

