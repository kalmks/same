package com.kalmks.same.data.parser;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetAttributeRequestDto {

    private String url;
    private String cssSelector;
    private String attribute;

}
