package com.kalmks.same.data.mail;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendMailRequestDto {

    private String recipient;
    private String subject;
    private String message;

}
