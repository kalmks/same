package com.kalmks.same.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "errorDto")
public class ErrorDto {

    private String message;

}
