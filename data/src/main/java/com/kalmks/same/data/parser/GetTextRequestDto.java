package com.kalmks.same.data.parser;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetTextRequestDto {

    private String url;
    private String cssSelector;

}
