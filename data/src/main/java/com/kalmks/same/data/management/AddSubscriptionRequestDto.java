package com.kalmks.same.data.management;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddSubscriptionRequestDto {

    private String  mail;
    private Integer artNo;

}
