package com.kalmks.same.discord.bot.saturn;

import com.kalmks.same.discord.config.domain.ManagementConfig;
import lombok.val;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SaturnListener extends ListenerAdapter {

    private final RestTemplate restTemplate;

    private final ManagementConfig config;

    public SaturnListener(RestTemplateBuilder restTemplateBuilder, ManagementConfig config) {
        restTemplate = restTemplateBuilder.build();
        this.config = config;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        val command = event.getMessage().getContentRaw();

        if (!command.toLowerCase().startsWith("!saturn")) {
            return;
        }

        if (command.matches("^!saturn(\\s)?$")) {
            event.getChannel().sendMessage("Usage: !saturn <Artikel-Nummer>").queue();

            return;
        }

        val artNo = getArtNoFromCommand(event.getMessage().getContentRaw());

        try {
            event.getChannel()
                 .sendMessage("The price of '" + getProductName(artNo) + "' available at Saturn is currently € " + getProductPrice(artNo) + ".")
                 .queue();
        } catch (Throwable e) {
            event.getChannel().sendMessage("Bing. bong. Something went").queue();
            event.getChannel().sendMessage("https://www.memesmonkey.com/images/memesmonkey/ff/ff7b42928c0824b46e4d267be9bd927c.jpeg").queue();
        }
    }

    private String getArtNoFromCommand(String command) {
        val artNo = command.substring(8);

        assert artNo.matches("^[0-9]{7}$");

        return artNo;
    }

    private String getProductName(final String artNo) {
        val name = restTemplate.getForObject(getGetNameUrl(artNo), String.class);

        assert name != null : "name is null";

        return name;
    }

    private String getProductPrice(final String artNo) {
        val price = restTemplate.getForObject(getGetPriceUrl(artNo), String.class);

        assert price != null : "price is null";

        return price;
    }

    private String getGetNameUrl(final String artNo) {
        return config.getUrl() + "/rest/saturn/" + artNo + "/name";
    }

    private String getGetPriceUrl(final String artNo) {
        return config.getUrl() + "/rest/saturn/" + artNo + "/price";
    }

}
