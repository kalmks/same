package com.kalmks.same.discord.bot.subscription;

import lombok.val;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

@Component
public class SubscribeListener extends ListenerAdapter {

    private final SubscriptionProxyService subscriptionProxyService;

    public SubscribeListener(final SubscriptionProxyService subscriptionProxyService) {
        this.subscriptionProxyService = subscriptionProxyService;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        val command = event.getMessage().getContentRaw();

        if (!command.toLowerCase().startsWith("!subscribe")) {
            return;
        }

        if (command.matches("^!subscribe(\\s)?$")) {
            event.getChannel().sendMessage("Usage: !subscribe <Artikel-Nummer> <E-Mail>").queue();

            return;
        }

        val artNo = getArtNoFromCommand(command);
        val mail = getMailFromCommand(command);

        try {
            subscriptionProxyService.send(Integer.valueOf(artNo), mail);
        } catch (Throwable e) {
            e.printStackTrace();
            event.getChannel().sendMessage("Bing. bong. Something went").queue();
            event.getChannel().sendMessage("https://www.memesmonkey.com/images/memesmonkey/ff/ff7b42928c0824b46e4d267be9bd927c.jpeg").queue();
            return;
        }

        event.getChannel()
             .sendMessage("You are now subscribed to " + artNo)
             .queue();
    }

    private String getMailFromCommand(final String command) {
        val mail = command.substring(18);

        assert mail.matches("^.+@.+\\..+$");

        return mail;
    }

    private String getArtNoFromCommand(String command) {
        val artNo = command.substring(11,18);

        assert artNo.matches("^[0-9]{7}$");

        return artNo;
    }

}

