package com.kalmks.same.discord.bot.bing;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class BingListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getMessage().getContentRaw().toLowerCase().startsWith("!bing")) {
            return;
        }

        event.getChannel().sendMessage("bong!").queue();
    }

}
