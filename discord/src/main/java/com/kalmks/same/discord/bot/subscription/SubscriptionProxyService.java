package com.kalmks.same.discord.bot.subscription;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kalmks.same.data.mail.SendMailResponseDto;
import com.kalmks.same.data.management.AddSubscriptionRequestDto;
import com.kalmks.same.discord.config.domain.ManagementConfig;
import lombok.val;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SubscriptionProxyService {

    private final RestTemplate     restTemplate;
    private final ManagementConfig managementConfig;

    public SubscriptionProxyService(final RestTemplateBuilder restTemplateBuilder, final ManagementConfig managementConfig) {
        restTemplate = restTemplateBuilder.build();
        this.managementConfig = managementConfig;
    }

    public void send(final Integer artNo, final String mail) {
        val request = AddSubscriptionRequestDto
            .builder()
            .artNo(artNo)
            .mail(mail)
            .build();
        val response = restTemplate.postForObject(getSendUrl(), request, Object.class);

        assert response != null : "response is null";
    }

    private String getSendUrl() {
        return managementConfig.getUrl() + "/rest/subscription/add";
    }
}
