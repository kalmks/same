package com.kalmks.same.discord.bot.mediamarkt;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MediaMarktListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getMessage().getContentRaw().toLowerCase().startsWith("!mediamarkt")) {
            return;
        }

        event.getChannel().sendMessage("The price of 'ArticleName' available at MediaMarkt is currently 69.00 Hu$o.").queue();
    }

}
