package com.kalmks.same.discord.bot;

import com.kalmks.same.discord.bot.bing.BingListener;
import com.kalmks.same.discord.bot.comparison.CompareListener;
import com.kalmks.same.discord.bot.mediamarkt.MediaMarktListener;
import com.kalmks.same.discord.bot.saturn.SaturnListener;
import com.kalmks.same.discord.bot.subscription.SubscribeListener;
import com.kalmks.same.discord.config.domain.DiscordConfig;
import lombok.val;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.BeansException;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import javax.security.auth.login.LoginException;

@Component
public class Bootstrap implements ApplicationContextAware, ApplicationListener<ApplicationReadyEvent> {

    private final DiscordConfig config;

    private ApplicationContext applicationContext;

    private SaturnListener saturnListener;

    private SubscribeListener subscribeListener;

    public Bootstrap(DiscordConfig config, SaturnListener saturnListener, SubscribeListener subscribeListener) {
        this.config = config;
        this.saturnListener = saturnListener;
        this.subscribeListener = subscribeListener;
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent applicationReadyEvent) {
        JDA api = initializeJda();

        waitForJdaReady(api);
    }

    private JDA initializeJda() {
        try {
            return JDABuilder
                    .createDefault(config.getToken())
                    .addEventListeners(getListeners())
                    .build();
        } catch (LoginException e) {
            shutdownApplication();
            return null;
        }
    }

    private ListenerAdapter[] getListeners() {
        return new ListenerAdapter[]{
                new BingListener(),
                saturnListener,
                new MediaMarktListener(),
                new CompareListener(),
                subscribeListener
        };
    }

    private void waitForJdaReady(final JDA api) {
        try {
            api.awaitReady();
        } catch (InterruptedException e) {
            shutdownApplication();
        }
    }

    private void shutdownApplication() {
        val configurableContext = (ConfigurableApplicationContext) applicationContext;
        configurableContext.close();
    }
}
