package com.kalmks.same.discord.bot.bing;

import lombok.val;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class BingListenerTest {

    @Mock
    private Message message;

    @Mock
    private MessageReceivedEvent event;

    private BingListener listener;

    @BeforeEach
    void setUp() {
        listener = new BingListener();
    }

    @ParameterizedTest
    @ValueSource(strings = {"!bing", "!Bing"})
    void onMessageReceived_shouldRespondIgnoringCase(final String command) {
        val channel       = mock(MessageChannel.class);
        val action        = mock(MessageAction.class);
        val messageCaptor = ArgumentCaptor.forClass(String.class);

        given(message.getContentRaw()).willReturn(command);
        given(channel.sendMessage(messageCaptor.capture())).willReturn(action);
        given(event.getMessage()).willReturn(message);
        given(event.getChannel()).willReturn(channel);

        listener.onMessageReceived(event);

        then(action)
            .should(times(1))
            .queue();
        assertThat(messageCaptor.getValue()).isEqualTo("bong!");
    }

    @Test
    void onMessageReceived_ignoresUnmatchedCommands() {
        given(message.getContentRaw()).willReturn("!notThisCommand");
        given(event.getMessage()).willReturn(message);

        listener.onMessageReceived(event);

        then(event)
            .should(times(0))
            .getChannel();
    }

}
