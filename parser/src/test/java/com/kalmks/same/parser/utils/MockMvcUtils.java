package com.kalmks.same.parser.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.kalmks.same.parser.utils.JsonUtils.objToJson;

public class MockMvcUtils {

    public static MockHttpServletRequestBuilder post(final String url, final Object payload) throws JsonProcessingException {
        return MockMvcRequestBuilders
            .post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objToJson(payload))
            .accept(MediaType.APPLICATION_JSON);
    }

}
