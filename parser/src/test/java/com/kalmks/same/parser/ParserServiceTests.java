package com.kalmks.same.parser;

import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ParserServiceTests {

    private static final String URL                 = "http://test.url/";
    private static final String SELECTOR            = "body div.bar";
    private static final String TEST_VALID_HTML     = "<html><body><p>bar</p><div class='bar' content='foo'>text</div></body></html>";
    private static final String TEST_AMBIGUOUS_HTML = "<html><body><p>bar</p><div class='bar'></div><div class='bar'></div></body></html>";
    private static final String TEST_UNMATCHED_HTML = "<html><body><p>bar</p></body></html>";

    @Mock
    private HttpService httpService;

    @InjectMocks
    private ParserService parserService;

    @Test
    void getPropertyValueOfElement_returnsProperty() throws UnmatchedSelectorException, AmbiguousSelectorException {
        setHtmlResponse(TEST_VALID_HTML);

        final String propertyVal = getContentAttrForSelector();

        assertThat(propertyVal).isEqualTo("foo");
    }

    @Test
    void getPropertyValueOfElement_throwsIfSelectorIsAmbiguous() {
        setHtmlResponse(TEST_AMBIGUOUS_HTML);

        assertThatThrownBy(this::getContentAttrForSelector)
            .isInstanceOf(AmbiguousSelectorException.class)
            .hasMessage("CSS selector is ambiguous");
    }

    @Test
    void getPropertyValueOfElement_throwsIfSelectorDoesNotMatch() {
        setHtmlResponse(TEST_UNMATCHED_HTML);

        assertThatThrownBy(this::getContentAttrForSelector)
            .isInstanceOf(UnmatchedSelectorException.class)
            .hasMessage("CSS selector does not match");
    }

    @Test
    void getTextOfElement_returnsProperty() throws UnmatchedSelectorException, AmbiguousSelectorException {
        setHtmlResponse(TEST_VALID_HTML);

        final String propertyVal = getTextForSelector();

        assertThat(propertyVal).isEqualTo("text");
    }

    @Test
    void getTextOfElement_throwsIfSelectorIsAmbiguous() {
        setHtmlResponse(TEST_AMBIGUOUS_HTML);

        assertThatThrownBy(this::getTextForSelector)
            .isInstanceOf(AmbiguousSelectorException.class)
            .hasMessage("CSS selector is ambiguous");
    }

    @Test
    void getTextOfElement_throwsIfSelectorDoesNotMatch() {
        setHtmlResponse(TEST_UNMATCHED_HTML);

        assertThatThrownBy(this::getTextForSelector)
            .isInstanceOf(UnmatchedSelectorException.class)
            .hasMessage("CSS selector does not match");
    }

    private void setHtmlResponse(final String htmlResponse) {
        given(httpService.getContent(URL)).willReturn(htmlResponse);
    }

    private String getContentAttrForSelector() throws UnmatchedSelectorException, AmbiguousSelectorException {
        return parserService.getAttributeValueOfElement(URL, SELECTOR, "content");
    }

    private String getTextForSelector() throws UnmatchedSelectorException, AmbiguousSelectorException {
        return parserService.getTextOfElement(URL, SELECTOR);
    }
}
