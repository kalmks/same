package com.kalmks.same.parser;

import com.kalmks.same.data.parser.GetAttributeRequestDto;
import com.kalmks.same.data.parser.GetTextRequestDto;
import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class ParserControllerTests {

    private static Stream<Arguments> selectorExceptionClassAndMessagePairs() {
        return Stream.of(
            Arguments.of(new UnmatchedSelectorException("unmatched")),
            Arguments.of(new AmbiguousSelectorException("ambiguous"))
        );
    }

    @Mock
    private ParserService service;

    @InjectMocks
    private ParserController controller;

    @Test
    void getAttribute_returnsStringFromService() throws UnmatchedSelectorException, AmbiguousSelectorException {
        val parsedText = "some important parsed text";
        val url        = "http://some.url";
        val selector   = ".selector";
        val attribute  = "attr";
        val request = GetAttributeRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .attribute(attribute)
            .build();

        given(service.getAttributeValueOfElement(url, selector, attribute)).willReturn(parsedText);

        val result = controller.getAttribute(request);

        then(service).should(times(1)).getAttributeValueOfElement(url, selector, attribute);
        assertThat(result.getResult()).isEqualTo(parsedText);
    }

    @ParameterizedTest
    @MethodSource("selectorExceptionClassAndMessagePairs")
    void getAttribute_throwsExceptions(final Exception exception) throws UnmatchedSelectorException, AmbiguousSelectorException {
        given(service.getAttributeValueOfElement(null, null, null)).willThrow(exception);

        assertThatThrownBy(() -> controller.getAttribute(GetAttributeRequestDto.builder().build()))
            .isEqualTo(exception);
    }

    @Test
    void getText_returnsStringFromService() throws UnmatchedSelectorException, AmbiguousSelectorException {
        val parsedText = "some important parsed text";
        val url        = "http://some.url";
        val selector   = ".selector";
        val request = GetTextRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .build();

        given(service.getTextOfElement(url, selector)).willReturn(parsedText);

        val result = controller.getText(request);

        then(service).should(times(1)).getTextOfElement(url, selector);
        assertThat(result.getResult()).isEqualTo(parsedText);
    }

    @ParameterizedTest
    @MethodSource("selectorExceptionClassAndMessagePairs")
    void getText_throwsExceptions(final Exception exception) throws UnmatchedSelectorException, AmbiguousSelectorException {
        given(service.getTextOfElement(null, null)).willThrow(exception);

        assertThatThrownBy(() -> controller.getText(GetTextRequestDto.builder().build()))
            .isEqualTo(exception);
    }

}
