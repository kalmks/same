package com.kalmks.same.parser;

import com.kalmks.same.data.parser.GetAttributeRequestDto;
import com.kalmks.same.data.parser.GetTextRequestDto;
import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.stream.Stream;

import static com.kalmks.same.parser.utils.MockMvcUtils.post;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ParserController.class)
public class ParserControllerWebMvcTests {

    private static final String GET_ATTRIBUTE = "/rest/parser/get-attribute";
    private static final String GET_TEXT = "/rest/parser/get-text";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ParserService service;

    private static Stream<Arguments> selectorExceptionClassAndMessagePairs() {
        return Stream.of(
            Arguments.of(new UnmatchedSelectorException("unmatched")),
            Arguments.of(new AmbiguousSelectorException("ambiguous"))
        );
    }

    @Test
    void getAttribute_returnsStringFromService() throws Exception {
        val parsedText = "some important parsed text";
        val url        = "http://some.url";
        val selector   = ".selector";
        val attribute  = "attr";
        val payload = GetAttributeRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .attribute(attribute)
            .build();
        val request = post(GET_ATTRIBUTE, payload);

        given(service.getAttributeValueOfElement(url, selector, attribute)).willReturn(parsedText);

        mockMvc.perform(request)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.result").value(parsedText));
    }

    @ParameterizedTest
    @MethodSource("selectorExceptionClassAndMessagePairs")
    void getAttribute_returnErrorDtos(final Exception exception) throws Exception {
        given(service.getAttributeValueOfElement(null, null, null)).willThrow(exception);

        mockMvc.perform(post(GET_ATTRIBUTE, GetAttributeRequestDto.builder().build()))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$.message").value(exception.getMessage()));
    }

    @Test
    void getText_returnsStringFromService() throws Exception {
        val parsedText = "some important parsed text";
        val url        = "http://some.url";
        val selector   = ".selector";
        val payload = GetTextRequestDto
            .builder()
            .url(url)
            .cssSelector(selector)
            .build();
        val request = post(GET_TEXT, payload);

        given(service.getTextOfElement(url, selector)).willReturn(parsedText);

        mockMvc.perform(request)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.result").value(parsedText));
    }

    @ParameterizedTest
    @MethodSource("selectorExceptionClassAndMessagePairs")
    void getText_returnErrorDtos(final Exception exception) throws Exception {
        given(service.getTextOfElement(null, null)).willThrow(exception);

        mockMvc.perform(post(GET_TEXT, GetTextRequestDto.builder().build()))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$.message").value(exception.getMessage()));
    }

}
