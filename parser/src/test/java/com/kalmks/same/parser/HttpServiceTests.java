package com.kalmks.same.parser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class HttpServiceTests {

    private static final String URL     = "http://test.url/";
    private static final String CONTENT = "test";

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate restTemplate;

    private HttpService httpService;

    @BeforeEach
    void setUp() {
        given(restTemplateBuilder.build()).willReturn(restTemplate);

        httpService = new HttpService(restTemplateBuilder);
    }

    @Test
    void getContent_shouldReturnContent() {
        given(restTemplate.getForObject(URL, String.class)).willReturn(CONTENT);

        final String content = httpService.getContent(URL);

        assertThat(content).isEqualTo(CONTENT);
    }

    @Test
    void getContent_throwsOnError() {
        final String              exceptionMessage = "This is a fatal exception";
        final RestClientException exception        = new RestClientException(exceptionMessage);

        given(restTemplate.getForObject(URL, String.class)).willThrow(exception);

        assertThatThrownBy(() -> httpService.getContent(URL))
            .isEqualTo(exception);
    }
}
