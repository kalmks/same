package com.kalmks.same.parser;

import com.kalmks.same.data.ErrorDto;
import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.kalmks.same.data.ErrorDto.errorDto;

@RestControllerAdvice
class ParserControllerAdvice {

    @ExceptionHandler(UnmatchedSelectorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorDto unmatchedSelectorHandler(final UnmatchedSelectorException exception) {
        return errorDto(exception.getMessage());
    }

    @ExceptionHandler(AmbiguousSelectorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorDto ambiguousSelectorHandler(final AmbiguousSelectorException exception) {
        return errorDto(exception.getMessage());
    }
}
