package com.kalmks.same.parser.exceptions;

public class UnmatchedSelectorException extends Exception {

    public UnmatchedSelectorException(final String message) {
        super(message);
    }
}
