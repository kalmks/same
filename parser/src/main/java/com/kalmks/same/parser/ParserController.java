package com.kalmks.same.parser;

import com.kalmks.same.data.parser.GetAttributeRequestDto;
import com.kalmks.same.data.parser.GetAttributeResponseDto;
import com.kalmks.same.data.parser.GetTextRequestDto;
import com.kalmks.same.data.parser.GetTextResponseDto;
import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import lombok.val;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
class ParserController {

    private final ParserService parserService;

    ParserController(final ParserService parserService) {
        this.parserService = parserService;
    }

    @PostMapping("/parser/get-attribute")
    GetAttributeResponseDto getAttribute(
        @RequestBody
        final GetAttributeRequestDto requestDto
    ) throws UnmatchedSelectorException, AmbiguousSelectorException {
        val result = parserService.getAttributeValueOfElement(
            requestDto.getUrl(),
            requestDto.getCssSelector(),
            requestDto.getAttribute()
        );

        return GetAttributeResponseDto
            .builder()
            .result(result)
            .build();
    }

    @PostMapping("/parser/get-text")
    GetTextResponseDto getText(
        @RequestBody
        final GetTextRequestDto requestDto
    ) throws UnmatchedSelectorException, AmbiguousSelectorException {
        val result = parserService.getTextOfElement(
            requestDto.getUrl(),
            requestDto.getCssSelector()
        );

        return GetTextResponseDto
            .builder()
            .result(result)
            .build();
    }
}
