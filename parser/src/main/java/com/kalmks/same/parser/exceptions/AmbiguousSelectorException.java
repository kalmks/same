package com.kalmks.same.parser.exceptions;

public class AmbiguousSelectorException extends Exception {

    public AmbiguousSelectorException(final String message) {
        super(message);
    }
}
