package com.kalmks.same.parser;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class HttpService {

    private final RestTemplate restTemplate;

    HttpService(final RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    String getContent(final String url) {
        return restTemplate.getForObject(url, String.class);
    }
}
