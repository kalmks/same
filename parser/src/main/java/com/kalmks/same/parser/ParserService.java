package com.kalmks.same.parser;

import com.kalmks.same.parser.exceptions.AmbiguousSelectorException;
import com.kalmks.same.parser.exceptions.UnmatchedSelectorException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
class ParserService {

    private final HttpService httpService;

    ParserService(final HttpService httpService) {
        this.httpService = httpService;
    }

    String getAttributeValueOfElement(
        final String url,
        final String cssSelector,
        final String attribute
    ) throws UnmatchedSelectorException, AmbiguousSelectorException {
        final String  content = httpService.getContent(url);
        final Element element = getUnambiguousElement(content, cssSelector);

        return element.attr(attribute);
    }

    String getTextOfElement(final String url, final String cssSelector) throws UnmatchedSelectorException, AmbiguousSelectorException {
        final String  content = httpService.getContent(url);
        final Element element = getUnambiguousElement(content, cssSelector);

        return element.text();
    }

    private Element getUnambiguousElement(final String content, final String cssSelector) throws AmbiguousSelectorException, UnmatchedSelectorException {
        final Document document = Jsoup.parse(content);
        final Elements elements = document.select(cssSelector);

        if (elements.size() > 1) {
            throw new AmbiguousSelectorException("CSS selector is ambiguous");
        } else if (elements.size() < 1) {
            throw new UnmatchedSelectorException("CSS selector does not match");
        }

        return elements.first();
    }
}
