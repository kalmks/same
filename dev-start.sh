#!/usr/bin/env bash

dev_dir=$(dirname $(realpath $0))/dev

args="--file ${dev_dir}/docker-compose.yml --project-name same"

docker-compose $args pull
docker-compose $args up --detach
